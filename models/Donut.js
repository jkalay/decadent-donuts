const mongoose = require('mongoose');

const donutSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  price: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String
  },
  description: {
    type: String,
    required: true
  },
  calories: {
    type: Number,
    required: true
  },
  kosher: Boolean
});

module.exports = mongoose.model('Donut', donutSchema);
