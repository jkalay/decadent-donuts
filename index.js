const express = require('express');
const app = express();
const mongoose = require('mongoose');
const donutRoutes = require('./routes/donutRoutes');
const boxRoutes = require('./routes/boxRoutes');

mongoose.Promise = global.Promise;

app.set('port', process.env.PORT || 5000);

app.use(express.json());

app.use('/api/donuts', donutRoutes);
app.use('/api/box', boxRoutes);

if (process.env.NODE_ENV === 'production') {
  const path = require('path');

  app.use(express.static('client/build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

(async () => {
  try {
    await mongoose.connect(
      process.env.MONGODB_URI || 'mongodb://localhost:27017/donuts',
      {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );
    console.log('Mongoose connected to MongoDB successfully!');
  } catch (err) {
    console.error('Could not connect to MongoDB', err);
  }
})();

app.listen(app.get('port'), () => {
  console.log(`Server is listening on port ${app.get('port')}`);
});
