import React, { Component } from 'react';
import axios from 'axios';

class DonutDetails extends Component {
  state = {
    donut: {}
  };

  getDonutDetails = async () => {
    try {
      const { id } = this.props.viewParams;
      const { data } = await axios.get(`/api/donuts/${id}`);
      this.setState({ donut: data });
    } catch (err) {
      console.log(err);
    }
  };

  goBack = () => {
    this.props.setView('catalog', { id: null });
  };

  componentDidMount() {
    this.getDonutDetails();
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8">
            <p className="text-muted" onClick={this.goBack}>
              <span style={{ cursor: 'pointer' }}>
                <i className="fas fa-arrow-left"></i> Back to donuts listing
              </span>
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 mb-4">
            <img
              className="border rounded img-fluid"
              src={this.state.donut.imageUrl}
              alt={this.state.donut.name}
            />
          </div>
          <div className="col-md-8">
            <h3 className="card-title">{this.state.donut.name}</h3>
            <button
              className="btn btn-custom mb-3"
              onClick={() => this.props.addToBox(this.state.donut)}
            >
              Add to Donut Box
            </button>
            <p className="text-success font-weight-bold">
              <i className="fas fa-dollar-sign"></i>
              {this.state.donut.price} / ea.
            </p>
            <p className="card-text">{this.state.donut.description}</p>
            <p className="card-text">
              <span className="badge badge-info mr-1">
                {this.state.donut.calories} Calories
              </span>

              {this.state.donut.kosher && (
                <span className="badge badge-info">Kosher</span>
              )}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default DonutDetails;
