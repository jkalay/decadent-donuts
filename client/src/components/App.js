import React, { Component } from 'react';
import axios from 'axios';
import Header from './Header';
import DonutList from './DonutList';
import DonutDetails from './DonutDetails';
import BoxSummary from './BoxSummary';
import CheckoutForm from './CheckoutForm';
import Footer from './Footer';
import './custom.css';

class App extends Component {
  state = {
    donuts: [],
    box: [],
    view: {
      name: 'catalog',
      params: {}
    }
  };

  getDonuts = async () => {
    try {
      const { data } = await axios.get('/api/donuts/');
      this.setState({ donuts: data });
    } catch (err) {
      console.log(err);
    }
  };

  getDonutBox = async () => {
    try {
      const { data } = await axios.get('/api/box/');
      this.setState({ box: data });
    } catch (err) {
      console.log(err);
    }
  };

  addToBox = async donut => {
    try {
      await axios.post('/api/box', donut);
      this.setState({ box: [...this.state.box, donut] });
    } catch (err) {
      console.log(err);
    }
  };

  setView = (name, params) => {
    this.setState({
      view: {
        name,
        params
      }
    });
  };

  placeOrder = async (email, phone, instructions) => {
    const orderDetails = {
      email,
      phone,
      instructions,
      box: this.state.box
    };

    try {
      await axios.post('/api/box/checkout', orderDetails);
      this.setState({ box: [] });
      this.setView('catalog', {});
    } catch (err) {
      console.error('Error placing order', err);
    }
  };

  renderView() {
    switch (this.state.view.name) {
      case 'catalog':
        return <DonutList donuts={this.state.donuts} setView={this.setView} />;
      case 'details':
        return (
          <DonutDetails
            viewParams={this.state.view.params}
            setView={this.setView}
            addToBox={this.addToBox}
          />
        );
      case 'box':
        return (
          <BoxSummary
            items={this.state.box}
            count={this.state.box.length}
            setView={this.setView}
          />
        );
      case 'checkout':
        return (
          <CheckoutForm
            items={this.state.box}
            placeOrder={this.placeOrder}
            setView={this.setView}
          />
        );
      default:
        return null;
    }
  }

  componentDidMount() {
    this.getDonuts();
    this.getDonutBox();
  }

  render() {
    return (
      <div className="container">
        <Header donutBoxCount={this.state.box.length} setView={this.setView} />
        <div className="container">{this.renderView()}</div>
        <Footer />
      </div>
    );
  }
}

export default App;
