import React from 'react';
import './BoxSummaryItem.css';

const BoxSummaryItem = props => {
  return (
    <>
      <div className="row">
        <div className="col-md-4 mb-4 text-center">
          <img
            className="rounded rounded-circle img-fluid box-item-img-custom"
            src={props.item.imageUrl}
            alt={props.item.name}
          />
        </div>
        <div className="col-md-8">
          <h4 style={{ fontWeight: 300 }}>{props.item.name}</h4>
          <p className="text-success font-weight-bold">
            <i className="fas fa-dollar-sign"></i>
            {props.item.price} / ea.
          </p>
        </div>
      </div>
    </>
  );
};

export default BoxSummaryItem;
