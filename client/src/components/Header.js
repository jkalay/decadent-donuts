import React from 'react';
import './Header.css';

const Header = props => {
  return (
    <header>
      <div className="container mb-3">
        <div className="row">
          <div className="col-12">
            <h1 className="text-center mt-2 mb-0 title-custom">
              <span
                onClick={() => props.setView('catalog', { id: null })}
                style={{ cursor: 'pointer' }}
              >
                Decadent Donuts
              </span>
            </h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center mb-2">
            <span
              className="nav-link-custom"
              onClick={() => props.setView('box', {})}
              style={{ cursor: 'pointer' }}
            >
              Your
              <i className="fas fa-box mx-1" style={{ color: '#ff7d97' }}></i>
              has
              <span className="mx-1 badge badge-secondary badge-custom">
                {props.donutBoxCount}
              </span>
              {props.donutBoxCount !== 1 ? 'donuts' : 'donut'}
            </span>
            {props.donutBoxCount > 0 && (
              <button
                className="btn btn-sm btn-custom ml-3"
                onClick={() => props.setView('checkout', {})}
              >
                Checkout
              </button>
            )}
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
