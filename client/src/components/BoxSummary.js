import React from 'react';
import BoxSummaryItem from './BoxSummaryItem';

const BoxSummary = props => {
  let items = props.items.map(item => (
    <BoxSummaryItem key={item.name} item={item} />
  ));

  let total = props.items.reduce((sum, item) => {
    sum += Number(item.price);
    return sum;
  }, 0);

  return (
    <div className="container">
      <div className="text-center">
        <h2 className="mb-3">
          <span style={{ borderBottom: '3px solid #f14668' }}>
            Your Donut Box
          </span>
        </h2>
        <div className="col-md-12 text-center mb-5">
          {props.count ? (
            <h4>Subtotal: ${total.toFixed(2)}</h4>
          ) : (
            <>
              <h4>Your box is empty</h4>
              <button
                className="btn btn-custom"
                onClick={() => props.setView('catalog', {})}
              >
                Start shopping
              </button>
            </>
          )}
        </div>
      </div>
      {items}
    </div>
  );
};

export default BoxSummary;
