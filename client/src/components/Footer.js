import React from 'react';

const Footer = () => {
  return (
    <footer className="text-center text-muted mt-4 mb-1">
      <small>
        <i class="fas fa-copyright"></i> 2020. Entirely fictional.
      </small>
    </footer>
  );
};

export default Footer;
