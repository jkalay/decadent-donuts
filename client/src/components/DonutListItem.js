import React from 'react';
import './DonutListItem.css';

const DonutListItem = props => {
  const details = props.details;
  return (
    <div className="card mb-3" style={{ minWidth: '250px' }}>
      <img
        src={details.imageUrl}
        alt={details.name}
        className="card-img-top img-fluid donut-image"
        onClick={() => props.setView('details', { id: details._id })}
      />
      <div className="card-body">
        <h4 className="card-title donut-name">
          <a href={details.imageUrl}>{details.name}</a>
        </h4>
        <p className="card-text">{details.description}</p>
      </div>
    </div>
  );
};

DonutListItem.defaultProps = {
  name: 'Donut'
};

export default DonutListItem;
