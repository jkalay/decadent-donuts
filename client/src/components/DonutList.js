import React from 'react';
import DonutListItem from './DonutListItem';

const DonutList = props => {
  const items = props.donuts.map(donut => {
    return (
      <DonutListItem key={donut.name} details={donut} setView={props.setView} />
    );
  });

  return <div className="card-deck">{items}</div>;
};

export default DonutList;
