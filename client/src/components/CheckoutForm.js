import React from 'react';
import './CheckoutForm.css';

class CheckoutForm extends React.Component {
  state = {
    email: '',
    instructions: '',
    phone: '',
    errorMessage: ''
  };

  getTotal = () => {
    return this.props.items.reduce((sum, item) => {
      sum += Number(item.price);
      return sum;
    }, 0);
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleOrder = () => {
    if (!this.state.email || !this.state.phone) {
      return this.setState({
        errorMessage: 'Invalid input. Please provide valid input and try again!'
      });
    }

    this.props.placeOrder(
      this.state.email,
      this.state.phone,
      this.state.instructions
    );
  };

  render() {
    return (
      <div className="container">
        <h2 className="text-center mb-4">
          <span style={{ borderBottom: '3px solid #f14668' }}>Checkout</span>
        </h2>
        <div className="row">
          <div className="col-md-12">
            {this.state.errorMessage && (
              <div className="text-center text-danger mb-2">
                <strong>Error:</strong> {this.state.errorMessage}
              </div>
            )}
            <form>
              <div className="form-group">
                <label htmlFor="inputEmail">Email address</label>
                <input
                  value={this.state.email}
                  onChange={this.handleChange}
                  className="form-control"
                  type="email"
                  name="email"
                  id="inputEmail"
                  placeholder="Enter email"
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputPhone">Phone number</label>
                <input
                  value={this.state.phone}
                  onChange={this.handleChange}
                  className="form-control"
                  type="text"
                  name="phone"
                  id="inputPhone"
                  placeholder="Enter phone number"
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputInstructions">
                  Additional instructions
                </label>
                <textarea
                  value={this.state.instructions}
                  onChange={this.handleChange}
                  className="form-control"
                  name="instructions"
                  id="inputInstructions"
                  rows="3"
                  placeholder="Enter additional instructions (delivery time, allergen info, etc.)"
                />
              </div>
            </form>
            <small className="text-muted">
              Please note that Decadent Donuts is entirely fictional and for
              demonstrative purposes only. Your order won't actually be placed!
            </small>
          </div>
        </div>
        <div className="row mt-3 mb-3">
          <div className="col-6">
            <h4>Subtotal: ${this.getTotal().toFixed(2)}</h4>
          </div>
          <div className="col-6 text-right">
            <button
              className="btn btn-custom"
              type="submit"
              onClick={this.handleOrder}
            >
              Place Order
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckoutForm;
