const router = require('express').Router();
const mongoose = require('mongoose');
const Donut = require('../models/Donut');
const donutsList = require('../dummy-donuts.json');
const donutDetails = require('../dummy-donut-details.json');

router.get('/', async (req, res) => {
  const donuts = await Donut.find();
  res.json(donuts);
});

router.get('/:id', async (req, res) => {
  const donut = await Donut.findById(req.params.id);
  res.json(donut);
});

router.post('/', (req, res) => {
  console.log(req.body);
});

module.exports = router;
