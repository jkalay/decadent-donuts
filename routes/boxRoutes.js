const router = require('express').Router();
const mongoose = require('mongoose');
const donutsList = require('../dummy-donuts.json');
const donutDetails = require('../dummy-donut-details.json');
const donutBox = require('../dummy-donut-box.json');

router.get('/', (req, res) => {
  res.json([]);
});

router.post('/', (req, res) => {
  console.log(req.body);
  res.status(201).send({
    success: true,
    message: 'Donut added to box successfully.'
  });
});

router.post('/checkout', (req, res) => {
  console.log(req.body);
  res.json({
    success: true,
    message: 'Your order has been placed!'
  });
});

module.exports = router;
