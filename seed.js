const mongoose = require('mongoose');
const Donut = require('./models/Donut');

mongoose
  .connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/donuts', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('Connected to MongoDB successfully'))
  .catch(err => console.log('Failed to connect to MongoDB', err));

mongoose.connection;

const assemblyLine = [];
const donuts = [
  {
    name: 'Bavarian Chocolate Cream',
    price: '2.99',
    description:
      'One of our most truly decadent offerings is filled with delicious Bavarian cream and topped with a chocolate glaze.',
    imageUrl: 'bavarian.png',
    calories: 270,
    kosher: false
  },
  {
    name: 'Traditional Glazed',
    price: '1.99',
    description:
      'This tried-and-true favorite is covered with just enough smooth glaze to make you get out of bed every morning.',
    imageUrl: 'traditionalglazed.jpg',
    calories: 200,
    kosher: true
  },
  {
    name: 'Cookie Monster',
    price: '4.99',
    description:
      'Royal blue icing topped with right out of the oven chocolate chip cookie crumbles, this one is always a truly "monstrous" hit with patrons.',
    imageUrl: 'cookiemonster.jpg',
    calories: 500,
    kosher: false
  },
  {
    name: 'Funfetti Sprinkle',
    price: '1.99',
    description:
      'Dipped in our own house-made strawberry icing and topped with colorful funfetti sprinkles.',
    imageUrl: 'funfetti.png',
    calories: 220,
    kosher: true
  },
  {
    name: 'Peanut Butter Cup',
    price: '2.99',
    description: 'Peanut butter icing topped with chocolate sprinkles.',
    imageUrl: 'peanutbutter.png',
    calories: 240,
    kosher: true
  },
  {
    name: 'Carrot Cake',
    price: '3.99',
    description:
      'A scrumptious morning treat, this combination of two baked favorites is topped with our mouthwatering cream cheese frosting.',
    imageUrl: 'carrotcake.png',
    calories: 450,
    kosher: false
  }
];

Donut.deleteMany()
  .exec()
  .then(() => {
    console.log('Deleted all donuts');
  });

donuts.forEach(donut => {
  assemblyLine.push(
    Donut.create({
      name: donut.name,
      price: donut.price,
      description: donut.description,
      imageUrl: donut.imageUrl,
      calories: donut.calories,
      kosher: donut.kosher
    })
  );
});

Promise.all(assemblyLine)
  .then(response => {
    console.log('Donut documents created successfully');
    console.log(JSON.stringify(response));
    mongoose.connection.close();
  })
  .catch(err => {
    console.log(`Error creating Donut documents: ${error}`);
  });
