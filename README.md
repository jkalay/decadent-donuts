# Decadent Donuts

A MERN stack virtual donut shop where prospective customers can shop for donuts, placing them in a virtual "donut box" and check out with their order.

## Technologies

- React.js
- Node.js (Express, axios, MongoDB/Mongoose)
- HTML5, CSS3 (Bootstrap), JS (ES6+)

## Planned additions

- Introduce routing with React Router
- Decrease prop drilling by managing state with Redux
